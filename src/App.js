import './App.css';
import TodoList from './features/todo/components/TodoList';

function App() {
  return (
    <div className="app">
      <TodoList></TodoList>
    </div>
  );
}
export default App;
